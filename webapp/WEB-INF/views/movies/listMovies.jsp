<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Peliculas</title>
	<spring:url value="/resources" var="urlPublic"/>
	<spring:url value="/movies/index" var="urlIndex" />
	<spring:url value="/movies/create" var="urlCreate" />
	<spring:url value="/movies/delete" var="urlDelete" />
	<spring:url value="/movies/edit" var="urlEdit" />
	<link rel="stylesheet" href="${urlPublic}/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${urlPublic}/bootstrap/css/theme.css">
</head>

<body>
	
	<jsp:include page="../includes/menu.jsp" />	
	
	<div class="container theme-showcase" role="main">
		
		<h3>Listado de Peliculas</h3>
		
		<c:if test="${mensaje!=null}">
			<div class="alert alert-success">
				${mensaje}
			</div>
		</c:if>
		
		<c:if test="${mensajeError!=null}">
			<div class="alert alert-danger">
				${mensajeError}
			</div>
		</c:if>
		
		<a href="${urlCreate}" class="btn btn-success" role="button" title="Nueva pelicula">Crear pelicula</a><br><br>
		
			<div class="table-responsive">
				<table class="table table-bordered table-hover">
					<thead>
						<tr>
							<th>Titulo</th>
							<th>Genero</th>
							<th>Clasificacion</th>
							<th>Duracion</th>
							<th>Fecha de Estreno</th>
							<th>Estado</th>
							<th>Opciones</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${listMovies.content}" var="movie">
							<tr>
								<td>${movie.titulo}</td>
								<td>${movie.genero}</td>
								<td>${movie.clasificacion}</td>
								<td>${movie.duracion}min</td>
								<td>
									<fmt:formatDate value="${movie.fechaEstreno}" pattern="dd-MM-yyyy"/>
								</td>
								<c:choose>
									<c:when test="${movie.estatus=='Activa'}">
										<td>
											<span class="label label-success">ACTIVA</span>
										</td>
									</c:when>
									<c:otherwise>
										<td>
											<span class="label label-danger">INACTIVA</span>
										</td>
									</c:otherwise>
								</c:choose>
								<td>
									<a href="${urlEdit}/${movie.id}" class="btn btn-success btn-sm" role="button" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>
									<a href="${urlDelete}/${movie.id}" class="btn btn-danger btn-sm" role="button" title="Eliminar"><span class="glyphicon glyphicon-trash"></span></a>
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
				<nav aria-label="">
					<ul class="pager">
						<c:if test="${listMovies.number > 0}">
							<li><a href="${urlIndex}?page=${listMovies.number - 1}">Anterior</a></li>
						</c:if>
						<c:if test="${listMovies.number < listMovies.totalPages -1}">
							<li><a href="${urlIndex}?page=${listMovies.number + 1}">Siguiente</a></li>
						</c:if>
					</ul>
				</nav>
			</div>
		
		<hr class="featurette-divider">
		
		<jsp:include page="../includes/footer.jsp" />
		
	</div>
	
	    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
    <script src="${urlPublic}/bootstrap/js/bootstrap.min.js"></script> 
    
</body>
</html>