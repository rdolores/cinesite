<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<title>CineSite | Bienvenido</title>
	
	<spring:url value="/resources" var="urlPublic" />
	<spring:url value="/news/create" var="urlCreate" />
	<spring:url value="/news/edit" var="urlEdit" />
	<spring:url value="/news/delete" var="urlDelete" />
	
	<link href="${urlPublic}/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="${urlPublic}/bootstrap/css/theme.css" rel="stylesheet">
	
</head>
<body>

	<jsp:include page="../includes/menu.jsp" />
	
	<div class="container theme-showcase" role="main">
	
	<c:if test="${mensaje!=null}">
		<div class="alert alert-success">
			${mensaje}
		</div>
	</c:if>
	
	<c:if test="${mensajeError!=null}">
		<div class="alert alert-danger">
			${mensajeError}
		</div>
	</c:if>
	
	<a href="${urlCreate}" class="btn btn-success" role="button" title="Create News">
		Create News
	</a>
	
	<br><br>
		<div class="table-responsive">
		
			<table class="table">
			
				<thead class="thead-dark">
					<tr>
						<th scope="col">Title</th>
						<th scope="col">Upload date</th>
						<th scope="col">Detail</th>
						<th scope="col">Status</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listNews}" var="news">
						<tr>
							<td>${news.titulo}</td>
							<td>
								<fmt:formatDate value="${news.dateOfNew}" pattern="dd-MM-yyyy"/>
							</td>
							<td style="width: 150px;">${news.detalle}</td>
							<td>${news.estatus}</td>
							<td>
								<a href="${urlEdit}/${news.id}" class="btn btn-info btn-sm" role="button" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>
								<a href="${urlDelete}/${news.id}" onclick=' return confirm("Estas seguro?") ' class="btn btn-dark btn-sm" role="button" title="Delete"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			
			</table>
		
		</div>
		
		<hr class="featurette-divider">
		
		<jsp:include page="../includes/footer.jsp" />
		
	</div>

</body>
</html>