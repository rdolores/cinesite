<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Banners</title>
	
	<spring:url value="/resources" var="urlPublic" />
	<spring:url value="/banners/index" var="urlIndex" />
	<spring:url value="/banners/create" var="urlCreate" />
	<spring:url value="/banners/edit" var="urlEdit" />
	<spring:url value="/banners/delete" var="urlDelete" />
	 
	<link rel="stylesheet" href="${urlPublic}/bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" href="${urlPublic}/bootstrap/css/theme.css">
</head>

<body>

	<jsp:include page="../includes/menu.jsp" />

	<div class="container theme-showcase" role="main">

		<h3>List of Banners</h3><br>
		
		<c:if test="${messageError!=null}">
			<div class="alert alert-danger">
				${messageError}
			</div>
		</c:if>
		
		<c:if test="${messageSuccess!=null}">
			<div class="alert alert-success">
				${messageSuccess}
			</div>
		</c:if><br>

		<a href="${urlCreate}" class="btn btn-success" role="button"
			title="New Banner">Create banner</a>
		<br><br>

		<div class="table-responsive">

			<table class="table">

				<thead class="thead-dark">
					<tr>
						<th scope="col">ID</th>
						<th scope="col">Title</th>
						<th scope="col">Upload Date</th>
						<th scope="col">State</th>
						<th scope="col">Options</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${listBanners.content}" var="banner">
						<tr>
							<th scope="row">${banner.id}</th>
							<td>${banner.title}</td>
							<td>
								<fmt:formatDate value="${banner.uploadDate}" pattern="dd-MM-yyyy"/>
							</td>
							<td>
								<c:choose>
									<c:when test="${banner.estatus=='Activo'}">
										<span class="label label-success">${banner.estatus}</span>
									</c:when>
									<c:otherwise>
										<span class="label label-danger">${banner.estatus}</span>
									</c:otherwise>
								</c:choose> 
							</td>
							<td>
								<a href="${urlEdit}/${banner.id}" class="btn btn-info btn-sm" role="button" title="Edit"><span class="glyphicon glyphicon-pencil"></span></a>
								<a href="${urlDelete}/${banner.id}" onclick=" return confirm('Esta seguro?') " class="btn btn-dark btn-sm" role="button" title="Eliminar"><span class="glyphicon glyphicon-trash"></span></a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<nav aria-label="">
				<ul class="pager">
					<c:if test="${listBanners.number > 0}">
						<li><a href="${urlIndex}?page=${listBanners.number - 1}">Anterior</a></li>
					</c:if>
					<c:if test="${listBanners.number < listBanners.totalPages - 1}">
						<li><a href="${urlIndex}?page=${listBanners.number + 1}">Siguiente</a></li>
					</c:if>
				</ul>
			</nav>

		</div>

		<hr class="featurette-divider">

		<jsp:include page="../includes/footer.jsp" />

	</div>

		    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
    <script src="${urlPublic}/bootstrap/js/bootstrap.min.js"></script> 

</body>

</html>