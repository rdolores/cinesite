<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">
	<title>Create Banner</title>
	
	<spring:url value="/resources" var="urlPublic" />
	<spring:url value="/banners/save" var="urlSave" />
	
	<link href="${urlPublic}/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="${urlPublic}/bootstrap/css/theme.css" rel="stylesheet">
	<link href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css" rel="stylesheet">

</head>
<body>

	<jsp:include page="../includes/menu.jsp" />

	<div class="container theme-showcase" role="main">

		<div class="page-header">
			<h3 class="blog-title">
				<span class="label label-success">Banner information</span>
			</h3>
		</div>
		
		<c:if test="${messageError!=null}">
			<div class="alert alert-danger" role="alert">
				${messageError}
			</div>
		</c:if>

		<form:form action="${urlSave}" method="post" enctype="multipart/form-data" modelAttribute="banner">

			<div class="row">
				<div class="col-sm-3">
					<div class="form-group">
						<label for="title">Title</label> 
						<form:hidden path="id" />
						<form:input type="text" class="form-control" path="title" id="title" required="required" />

					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">

						<label for="uploadDate">Upload date</label> 
						<form:input type="text" class="form-control" path="uploadDate" id="uploadDate" required="required" />

					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
						
						<label for="estatus" class="control-label">Estatus</label>
						<form:select path="estatus" class="form-control" id="estatus" required="required">
							<form:option value="Activo">Activo</form:option>
							<form:option value="Inactivo">Inactivo</form:option>
						</form:select>
						
					</div>
				</div>
				<div class="col-sm-3">
					<div class="form-group">
					
						<label for="image">Image</label>
						<input type="file" id="imageFile" name="imageFile">
						<form:hidden path="image"/>
						<p class="help-block">Banner's image</p>
					
					</div>
				</div>

			</div>
			
			<button type="submit" class="btn btn-danger" >Guardar</button>

		</form:form>

		<hr class="featurette-divider">
		
		<jsp:include page="../includes/footer.jsp" />

	</div>
	
	<!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> 
    <script src="${urlPublic}/bootstrap/js/bootstrap.min.js"></script> 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
      $(function () {
          $("#uploadDate").datepicker({dateFormat: 'dd-mm-yy'});
        }
      );
    </script>

</body>
</html>