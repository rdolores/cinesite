<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<spring:url value="/" var="urlRoot"/>

<!-- Fixed navbar -->
<nav class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
	
		<security:authorize access="isAnonymous()">
		
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${urlRoot}">My CineSite</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="#">Acerca</a></li>
				<li><a href="${urlRoot}admin/formLogin">Login</a></li>
			</ul>
		</div>
		
		</security:authorize>
		
		<security:authorize access="hasAnyAuthority('EDITOR')">
		
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${urlRoot}admin/index">My CineSite | Administración</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="${urlRoot}movies/index?page=0">Peliculas</a></li>
				<li><a href="${urlRoot}schedules/create">Horarios</a></li>
				<li><a href="${urlRoot}news/index">Noticias</a></li>
				<li><a href="${urlRoot}admin/logout">Logout</a></li>
			</ul>
		</div>
		
		</security:authorize>
		
		<security:authorize access="hasAnyAuthority('GERENTE')">
		
		<div class="navbar-header">
			<button type="button" class="navbar-toggle collapsed"
				data-toggle="collapse" data-target="#navbar" aria-expanded="false"
				aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="${urlRoot}admin/index">My CineSite | Administración</a>
		</div>
		<div id="navbar" class="navbar-collapse collapse">
			<ul class="nav navbar-nav">
				<li><a href="${urlRoot}movies/index?page=0">Peliculas</a></li>
				<li><a href="${urlRoot}banners/index?page=0">Banners</a></li>
				<li><a href="${urlRoot}schedules/create">Horarios</a></li>
				<li><a href="${urlRoot}news/index">Noticias</a></li>
				<li><a href="${urlRoot}admin/logout">Logout</a></li>
			</ul>
		</div>
		
		</security:authorize>
		<!--/.nav-collapse -->
	</div>
</nav>