package pruebasquery;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import net.ronaldfdg.model.Schedule;
import net.ronaldfdg.repository.ScheduleRepository;

public class ScheduleAppJPA {

	public static void main(String[] args) {
		 
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		ScheduleRepository repository = context.getBean("scheduleRepository", ScheduleRepository.class);
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			for(Schedule schedule : repository.findByDate(sdf.parse("2020-03-31")))
				System.out.println(schedule.getMovie());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		/*List<Schedule> listSchedule = repository.findAll();
		
		for(Schedule schedule : listSchedule)
			System.out.println(schedule);*/
		
		context.close();

	}

}
