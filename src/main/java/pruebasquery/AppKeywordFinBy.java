package pruebasquery;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import net.ronaldfdg.model.News;
import net.ronaldfdg.repository.NewsRepository;

public class AppKeywordFinBy {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		NewsRepository repository = context.getBean("newsRepository", NewsRepository.class);
		
		// List<News> listNewsByEstatus = repository.findByEstatus("Inactiva");
		
		List<News> listNewsByDate = null;
		
		try {
			listNewsByDate = repository.findByDateOfNew(sdf.parse("2020-02-25"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		for(News news : listNewsByDate)
			System.out.println(news);
		
		context.close();
		
	}

}
