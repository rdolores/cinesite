package pruebasquery;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import net.ronaldfdg.model.News;
import net.ronaldfdg.repository.NewsRepository;

public class AppKeywordBetween {

	public static void main(String[] args) {
		
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		NewsRepository repository = context.getBean("newsRepository", NewsRepository.class);
		
		List<News> listNews = null;
		
		try {
			listNews = repository.findByDateOfNewBetween(sdf.parse("2017-09-03"), sdf.parse("2020-12-06"));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		for(News news : listNews)
			System.out.println(news);
		
		System.out.println(listNews.size());
		
		context.close();

	}

}
