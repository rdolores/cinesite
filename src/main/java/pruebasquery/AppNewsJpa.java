package pruebasquery;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import net.ronaldfdg.model.News;
import net.ronaldfdg.repository.NewsRepository;

public class AppNewsJpa {

	public static void main(String[] args) {
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		NewsRepository repository = context.getBean("newsRepository", NewsRepository.class);
		
		for(News news : repository.findTop3ByEstatusOrderByDateOfNewDesc("Activa"))
			System.out.println(news);
		
		context.close();
	}

}
