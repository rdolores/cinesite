package pruebasquery;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Sort;

import net.ronaldfdg.model.News;
import net.ronaldfdg.repository.NewsRepository;

public class AppKeywordAnd {

	public static void main(String[] args) {
		 
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		NewsRepository repository = context.getBean("newsRepository", NewsRepository.class);
		
		List<News> listNewsByEstatusAndDate = null;
		
		try {
			listNewsByEstatusAndDate = repository.findByEstatusAndDateOfNew("Activa", sdf.parse("2020-02-25"),
																			Sort.by("titulo").ascending());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		for(News news : listNewsByEstatusAndDate)
			System.out.println(news);
		
		context.close();

	}

}
