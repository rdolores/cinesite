package pruebasjparepo;

import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Sort;

import net.ronaldfdg.model.News;
import net.ronaldfdg.repository.NewsRepository;

public class AppSorting {

	public static void main(String[] args) {
		 
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		NewsRepository repository = context.getBean("newsRepository", NewsRepository.class);
		
//		List<News> listNews = repository.findAll(Sort.by("titulo"));
		List<News> listNews = repository.findAll(Sort.by("dateOfNew").descending().and(Sort.by("titulo").ascending()));
		
		for(News news : listNews)
			System.out.println(news);
		
		context.close();

	}

}
