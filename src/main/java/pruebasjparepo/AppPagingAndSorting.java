package pruebasjparepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import net.ronaldfdg.model.News;
import net.ronaldfdg.repository.NewsRepository;

public class AppPagingAndSorting {

	public static void main(String[] args) {
		 
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		NewsRepository repository = context.getBean("newsRepository", NewsRepository.class);
		
		Page<News> newsPage = repository.findAll(PageRequest.of(0, 10, Sort.by("titulo")));
		
		for(News news : newsPage)
			System.out.println(news);
		
		context.close();
	}

}
