package pruebasjparepo;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import net.ronaldfdg.model.News;
import net.ronaldfdg.repository.NewsRepository;

public class AppPaging {

	public static void main(String[] args) {
		 
		ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("root-context.xml");
		
		NewsRepository repository = context.getBean("newsRepository", NewsRepository.class);
		
		Page<News> newsPage = repository.findAll(PageRequest.of(0, 5));
		
		for(News news : newsPage)
			System.out.println(news);

		context.close();
		
	}

}
