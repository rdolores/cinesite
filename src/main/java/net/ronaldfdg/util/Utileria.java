package net.ronaldfdg.util;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.multipart.MultipartFile;

public class Utileria {

	// Variable para agregar los dias que siguen a una determinada fecha
	static final int NEXT_DAYS = 4;

	// Variable para generar una cantidad de caracteres aleatorios
	static final int CANTIDAD_CARACTERES = 8;

	public static List<String> getNextDays() {

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

		Date start = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, NEXT_DAYS);
		Date endDate = calendar.getTime();

		GregorianCalendar gcal = new GregorianCalendar();
		gcal.setTime(start);
		List<String> nextDays = new ArrayList<>();

		while (!gcal.getTime().after(endDate)) {
			Date date = gcal.getTime();
			gcal.add(Calendar.DATE, 1);
			nextDays.add(sdf.format(date));
		}

		return nextDays;
	}

	public static String saveImage(MultipartFile multipart, HttpServletRequest request) {

		String imageFile = replaceSpacing(generateCharacters() + multipart.getOriginalFilename());
		String absolutePath = request.getServletContext().getRealPath("/resources/images/");

		try {

			File file = new File(absolutePath + imageFile);
			multipart.transferTo(file);
			return imageFile;

		} catch (IOException e) {
			System.out.print(e.getMessage());
			return null;
		}

	}

	public static String generateCharacters() {

		char character = ' ';
		StringBuilder builder = new StringBuilder();
		int counter = 0;

		while (counter < CANTIDAD_CARACTERES) {

			int opcion = (int) (Math.random() * 2);

			if (opcion == 0)
				character = (char) (int) (Math.random() * 26 + 65);
			else
				character = (char) (int) (Math.random() * 10 + 48);

			counter++;
			builder.append(character);
		}

		return builder.toString();
	}

	public static String replaceSpacing(String imageFile) {
		imageFile = imageFile.replace(" ", "-");
		return imageFile;
	}

}
