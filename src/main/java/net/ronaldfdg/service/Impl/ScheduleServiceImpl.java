package net.ronaldfdg.service.Impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ronaldfdg.model.Schedule;
import net.ronaldfdg.repository.ScheduleRepository;
import net.ronaldfdg.service.IScheduleService;

@Service
public class ScheduleServiceImpl implements IScheduleService{
	
	@Autowired
	private ScheduleRepository scheduleRepository;
	
	@Override
	public void save(Schedule schedule) {
		scheduleRepository.save(schedule);
	}

	@Override
	public List<Schedule> getScheduleByMovieAndDate(int idMovie, Date date) {
		List<Schedule> listSchedules = null;
		listSchedules = scheduleRepository.findByMovie_IdAndDateOrderByTime(idMovie, date);
		return listSchedules;
	}
	
}
