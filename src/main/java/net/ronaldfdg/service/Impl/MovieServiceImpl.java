package net.ronaldfdg.service.Impl;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.ronaldfdg.model.Movie;
import net.ronaldfdg.model.Schedule;
import net.ronaldfdg.repository.MovieRepository;
import net.ronaldfdg.repository.ScheduleRepository;
import net.ronaldfdg.service.IMovieService;

@Service
public class MovieServiceImpl implements IMovieService {
	
	@Autowired
	private MovieRepository movieRepository;
	
	@Autowired
	private ScheduleRepository scheduleRepository;
	
	@Override
	public Page<Movie> getMoviesForPage(Pageable page) {
		return movieRepository.findAll(page);
	}
	
	@Override
	public List<Movie> listMovies() {
		return movieRepository.findAll();
	}

	@Override
	public Movie getMovieById(int idMovie) {
		Optional<Movie> optional = movieRepository.findById(idMovie);
		if(optional.isPresent()) {
			Movie movie = optional.get();
			return movie;
		}
		return null;
	}

	@Override
	public void save(Movie movie) {
		movieRepository.save(movie);
	}
	
	@Override
	public void deleteById(int idMovie) {
		movieRepository.deleteById(idMovie);
	}

	@Override
	public List<String> getKindOfMovies() {
		
		List<String> listKindOfMovies = new LinkedList<>();
		listKindOfMovies.add("Accion");
		listKindOfMovies.add("Aventura");
		listKindOfMovies.add("Clasicas");
		listKindOfMovies.add("Comedia Romantica");
		listKindOfMovies.add("Drama");
		listKindOfMovies.add("Infantil");
		listKindOfMovies.add("Terror");
		listKindOfMovies.add("Accion y Aventura");
		listKindOfMovies.add("Romantica");
		
		return listKindOfMovies;
	}

	@Override
	public boolean existMovie(int idMovie) {
		return movieRepository.existsById(idMovie);
	}

	@Override
	public List<Movie> listMoviesByDate(Date date) {
		List<Movie> moviesByDate = null;
		List<Schedule> scheduleMovieByDate = scheduleRepository.findByDate(date);
		moviesByDate = new LinkedList<>();
		
		for(Schedule schedule : scheduleMovieByDate) 
			moviesByDate.add(schedule.getMovie());
		
		return moviesByDate;
	}

}
