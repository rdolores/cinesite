package net.ronaldfdg.service.Impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ronaldfdg.model.News;
import net.ronaldfdg.repository.NewsRepository;
import net.ronaldfdg.service.INewsService;

@Service
public class NewsServiceImpl implements INewsService {
	
	@Autowired
	private NewsRepository newsRepository;
	
	@Override
	public List<News> getListNews() { 
		return newsRepository.findAll();
	}
	
	@Override
	public void save(News news) {
		newsRepository.save(news);	
	}

	@Override
	public News getNewsById(int idNews) {
		
		Optional<News> optional = newsRepository.findById(idNews);
		
		if(optional.isPresent()) {
			News news = optional.get();
			return news;
		}
		
		return null;
	}

	@Override
	public void deleteById(int idNews) {
		newsRepository.deleteById(idNews);
	}

	@Override
	public boolean existNews(int idNews) {
		
		if(newsRepository.existsById(idNews))
			return true;
		
		return false;
	}

	@Override
	public List<News> findTop3ByEstatus(String estatus) {
		return newsRepository.findTop3ByEstatusOrderByDateOfNewDesc(estatus);
	}

}
