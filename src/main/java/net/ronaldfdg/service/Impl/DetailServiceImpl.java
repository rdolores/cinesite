package net.ronaldfdg.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ronaldfdg.model.Detail;
import net.ronaldfdg.repository.DetailRepository;
import net.ronaldfdg.service.IDetailService;

@Service
public class DetailServiceImpl implements IDetailService {

	@Autowired
	private DetailRepository detailRepository;
	
	@Override
	public void save(Detail detail) {
		detailRepository.save(detail);
	}

	@Override
	public void delete(int idDetail) {
		detailRepository.deleteById(idDetail);
	}

}
