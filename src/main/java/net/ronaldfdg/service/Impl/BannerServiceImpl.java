package net.ronaldfdg.service.Impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import net.ronaldfdg.model.Banner;
import net.ronaldfdg.repository.BannerRepository;
import net.ronaldfdg.service.IBannerService;

@Service
public class BannerServiceImpl implements IBannerService{
		
	@Autowired
	private BannerRepository repositoryBanner;
	
	@Override
	public void save(Banner banner) {
		repositoryBanner.save(banner);
	}

	@Override
	public List<Banner> getBanners() {
		return repositoryBanner.findAll();
	}
	
	@Override
	public boolean existsById(int id) {
		return repositoryBanner.existsById(id);
	}

	@Override
	public void deleteById(int id) {
		repositoryBanner.deleteById(id);
	}

	@Override
	public Banner findById(int id) {
		if(!repositoryBanner.findById(id).isPresent())
			return null;
		
		return repositoryBanner.findById(id).get();
	}

	@Override
	public Page<Banner> getPagesBanner(Pageable page) {
		return repositoryBanner.findAll(page);
	}
		
}
