package net.ronaldfdg.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ronaldfdg.model.User;
import net.ronaldfdg.repository.UserRepository;
import net.ronaldfdg.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository repositoryUser;
	
	@Override
	public void save(User user) {
		repositoryUser.save(user);
	}
		
}
