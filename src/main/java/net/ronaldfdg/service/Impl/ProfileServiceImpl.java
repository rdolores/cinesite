package net.ronaldfdg.service.Impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.ronaldfdg.model.Profile;
import net.ronaldfdg.repository.ProfileRepository;
import net.ronaldfdg.service.IProfileService;

@Service
public class ProfileServiceImpl implements IProfileService {

	@Autowired
	private ProfileRepository repositoryProfile;
	
	@Override
	public void save(Profile profile) {
		repositoryProfile.save(profile);
	}

}
