package net.ronaldfdg.service.Impl;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.springframework.stereotype.Service;

import net.ronaldfdg.model.Contact;
import net.ronaldfdg.service.IContactService;

@Service
public class ContactServiceImpl implements IContactService {
	
	List<Contact> listOfContacts = null;

	public ContactServiceImpl() {
		listOfContacts = new ArrayList<>();
	}
	
	@Override
	public void save(Contact contact) {
		listOfContacts.add(contact);
	}

	@Override
	public List<Contact> getListContacts() {
		return listOfContacts;
	}

	@Override
	public List<String> getKindOfNotifications() {
		
		List<String> kindOfNotifications = new LinkedList<>();
		
		kindOfNotifications.add("Estrenos");
		kindOfNotifications.add("Promociones");
		kindOfNotifications.add("Noticias");
		kindOfNotifications.add("Preventas");
		
		return kindOfNotifications;
	}

}
