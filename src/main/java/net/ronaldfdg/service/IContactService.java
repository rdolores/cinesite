package net.ronaldfdg.service;

import java.util.List;

import net.ronaldfdg.model.Contact;

public interface IContactService {
	
	void save(Contact contact);
	List<Contact> getListContacts(); 
	List<String> getKindOfNotifications();
}
