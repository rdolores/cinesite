package net.ronaldfdg.service;

import java.util.Date;
import java.util.List;

import net.ronaldfdg.model.Schedule;

public interface IScheduleService {
	
	void save(Schedule schedule);
	List<Schedule> getScheduleByMovieAndDate(int idMovie, Date date);
}
