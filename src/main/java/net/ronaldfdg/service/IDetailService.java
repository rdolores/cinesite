package net.ronaldfdg.service;

import net.ronaldfdg.model.Detail;

public interface IDetailService {

	void save(Detail detail);
	void delete(int idDetail);
	
}
