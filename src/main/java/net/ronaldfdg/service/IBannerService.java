package net.ronaldfdg.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.ronaldfdg.model.Banner;

public interface IBannerService {

	void save(Banner banner);
	void deleteById(int id);
	Banner findById(int id);
	boolean existsById(int id);
	List<Banner> getBanners();
	Page<Banner> getPagesBanner(Pageable page);
	
}
