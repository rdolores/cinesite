package net.ronaldfdg.service;

import java.util.List;

import net.ronaldfdg.model.News;

public interface INewsService {

	List<News> getListNews();
	void save(News news);
	News getNewsById(int idNews);
	void deleteById(int idNews);
	boolean existNews(int idNews);
	List<News> findTop3ByEstatus(String estatus);
	
}
