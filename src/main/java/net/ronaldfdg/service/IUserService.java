package net.ronaldfdg.service;

import net.ronaldfdg.model.User;

public interface IUserService {

	void save(User user);
	
}
