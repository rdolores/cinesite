package net.ronaldfdg.service;

import net.ronaldfdg.model.Profile;

public interface IProfileService {

	void save(Profile profile);
	
}
