package net.ronaldfdg.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import net.ronaldfdg.model.Movie;

public interface IMovieService {

	List<Movie> listMovies();
	Page<Movie> getMoviesForPage(Pageable page);
	Movie getMovieById(int idMovie);
	boolean existMovie(int idMovie);
	void save(Movie movie);
	List<String> getKindOfMovies();
	void deleteById(int idMovie);
	List<Movie> listMoviesByDate(Date date);
	
}
