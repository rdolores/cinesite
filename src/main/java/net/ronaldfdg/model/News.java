package net.ronaldfdg.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Noticias")
public class News {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	private String titulo;
	
	@Column(name = "fecha", nullable = false)
	private Date dateOfNew;
	
	private String detalle;
	private String estatus;

	public News() {
		this.dateOfNew = new Date();
		this.estatus = "Activa";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Date getDateOfNew() {
		return dateOfNew;
	}

	public void setDateOfNew(Date dateOfNew) {
		this.dateOfNew = dateOfNew;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		return "News [id=" + id + ", titulo=" + titulo + ", dateOfNew=" + dateOfNew + ", detalle=" + detalle
				+ ", estatus=" + estatus + "]";
	}

}
