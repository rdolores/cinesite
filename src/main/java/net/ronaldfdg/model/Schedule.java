package net.ronaldfdg.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Horarios")
public class Schedule {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	@Column(name="fecha", nullable=false)
	private Date date;
	@Column(name="hora", nullable=false)
	private String time;
	@Column(name="sala", nullable=false)
	private String room;
	@Column(name="precio", nullable=false)
	private double price;
	
	@ManyToOne
	@JoinColumn(name="idPelicula")
	private Movie movie;
	
	public Schedule() {
		
	}

	public int getId() {
		return id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getTime() {
		return time;
	}

	public void setTime(String time) {
		this.time = time;
	}

	public String getRoom() {
		return room;
	}

	public void setRoom(String room) {
		this.room = room;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Movie getMovie() {
		return movie;
	}

	public void setMovie(Movie movie) {
		this.movie = movie;
	}

	@Override
	public String toString() {
		return "Schedule [id=" + id + ", date=" + date + ", time=" + time + ", room=" + room + ", price=" + price + "]";
	}

}
