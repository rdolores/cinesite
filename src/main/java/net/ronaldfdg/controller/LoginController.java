package net.ronaldfdg.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("admin")
public class LoginController {

	@GetMapping("/formLogin")
	public String showFormLogin() {
		return "login/login";
	}
	
	@GetMapping("/logout")
	public String logout(HttpServletRequest request) {
		SecurityContextLogoutHandler logoutHandler = new SecurityContextLogoutHandler();
		logoutHandler.logout(request, null, null);
		return "redirect:/";
	}
	
	@GetMapping("/index")
	public String showAdminMain(Authentication authentication, Model model) {
		String nameFirstLetterUpper = authentication.getName().substring(0,1).toUpperCase() + 
										authentication.getName().substring(1);
		model.addAttribute("username", nameFirstLetterUpper);
		return "login/admin";
	}
	
}
