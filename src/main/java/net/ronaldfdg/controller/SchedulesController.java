package net.ronaldfdg.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.ronaldfdg.model.Movie;
import net.ronaldfdg.model.Schedule;
import net.ronaldfdg.service.IMovieService;
import net.ronaldfdg.service.IScheduleService;

@Controller
@RequestMapping("/schedules")
public class SchedulesController {

	@Autowired
	private IScheduleService scheduleService;
	
	@Autowired
	private IMovieService movieService;
	
	@GetMapping(value="/create")
	public String create(@ModelAttribute Schedule schedule) {
		return "schedules/formSchedule";
	}
	
	@PostMapping(value="/save")
	public String save(@ModelAttribute Schedule schedule, BindingResult result, RedirectAttributes attributes, Model model) {
		
		if(result.hasErrors()) {
			model.addAttribute("messageError","Hubo un error. Intentelo nuevamente");
			return "schedules/formSchedule";
		}
		
		scheduleService.save(schedule);
		
		attributes.addFlashAttribute("messageSuccess", "Horario creado correctamente");
		
		return "redirect:/schedules/create";
	}
	
	@ModelAttribute("listMovies")
	public List<Movie> getListMovies(){
		return movieService.listMovies();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	
}
