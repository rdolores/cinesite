package net.ronaldfdg.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.ronaldfdg.model.Contact;
import net.ronaldfdg.service.IContactService;
import net.ronaldfdg.service.IMovieService;

@Controller
public class ContactController {

	@Autowired
	private IMovieService movieService;
	
	@Autowired
	private IContactService contactService;
	
	@GetMapping("/contact")
	public String create(@ModelAttribute Contact contact, Model model) {
		
		List<String> kindOfMovies = movieService.getKindOfMovies();
		List<String> kindOfNotifications = contactService.getKindOfNotifications();
		
		model.addAttribute("kindOfMovies", kindOfMovies);
		model.addAttribute("kindOfNotifications", kindOfNotifications);
		
		return "contact/formContact";
		
	}
	
	@PostMapping("/contact")
	public String save(@ModelAttribute Contact contact, RedirectAttributes attribute) {
		
		attribute.addFlashAttribute("message", "El mensaje fue enviado");
		
		contactService.save(contact);
		
		return "redirect:/contact";
	}
	
}
