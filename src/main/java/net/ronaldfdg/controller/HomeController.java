package net.ronaldfdg.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import net.ronaldfdg.model.Banner;
import net.ronaldfdg.model.Movie;
import net.ronaldfdg.model.News;
import net.ronaldfdg.model.Schedule;
import net.ronaldfdg.service.IBannerService;
import net.ronaldfdg.service.IMovieService;
import net.ronaldfdg.service.INewsService;
import net.ronaldfdg.service.IScheduleService;
import net.ronaldfdg.util.Utileria;

@Controller
public class HomeController {
	
	@Autowired
	private IMovieService serviceMovie;
	
	@Autowired
	private IBannerService serviceBanner;
	
	@Autowired
	private IScheduleService serviceSchedule;
	
	@Autowired
	private INewsService serviceNews;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String goHome(Model model) {

		try {
			
			Date currentDate = sdf.parse(sdf.format(new Date()));
			List<Movie> movieList = serviceMovie.listMoviesByDate(currentDate);
			List<String> nextDaysFromToday = Utileria.getNextDays();
			List<Banner> bannerList = serviceBanner.getBanners();
			List<News> newsList = serviceNews.findTop3ByEstatus("Activa");
			
			model.addAttribute("movieList", movieList);
			model.addAttribute("bannerList", bannerList);
			model.addAttribute("nextDaysFromToday", nextDaysFromToday);
			model.addAttribute("fechaBusqueda", sdf.format(new Date()));
			model.addAttribute("newsList", newsList);
			
		} catch (ParseException e) {
			System.out.println("ERROR: HomeController.goHome: "+e.getMessage());
		}


		return "home";
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	public String searchMovieByDate(@RequestParam("fechaBusqueda") String fechaBusqueda, Model model) {
		
		try {
			
			List<Movie> movieList = serviceMovie.listMoviesByDate(sdf.parse(fechaBusqueda));
			List<String> nextDaysFromToday = Utileria.getNextDays();
			List<Banner> bannerList = serviceBanner.getBanners();
			List<News> newsList = serviceNews.findTop3ByEstatus("Activa");
			
			model.addAttribute("movieList", movieList);
			model.addAttribute("bannerList", bannerList);
			model.addAttribute("nextDaysFromToday", nextDaysFromToday);
			model.addAttribute("fechaBusqueda", fechaBusqueda);
			model.addAttribute("newsList", newsList);
			
		} catch (ParseException e) {
			System.out.println("ERROR: HomeController.searchMovieByDate.- " + e.getMessage());
		}
		
		return "home";
	}

	@RequestMapping(value = "/detail", method = RequestMethod.GET)
	public String detail(Model model, @RequestParam("idMovie") int idMovie, @RequestParam("fecha") Date date) {
		
		Movie movie = serviceMovie.getMovieById(idMovie);
		
		List<Schedule> schedulesMovie = serviceSchedule.getScheduleByMovieAndDate(idMovie, date);
		
		model.addAttribute("movie", movie);
		model.addAttribute("fechaBusqueda", sdf.format(date));
		model.addAttribute("schedulesMovie",schedulesMovie);
		
		return "movies/detailMovie";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}
	
}
