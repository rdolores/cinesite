package net.ronaldfdg.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import net.ronaldfdg.model.Profile;
import net.ronaldfdg.model.User;
import net.ronaldfdg.service.IProfileService;
import net.ronaldfdg.service.IUserService;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Autowired
	private IUserService serviceUser;
	
	@Autowired
	private IProfileService serviceProfile;
	
	@GetMapping("/index")
	public String index() {
		return "users/listUsers";
	}
	
	@GetMapping("/create")
	public String create(@ModelAttribute User user) {
		return "users/formUser";
	}
	
	@PostMapping("/save")
	public String save(@ModelAttribute User user, @RequestParam("profile") String profileName) {
		
		String password = user.getPassword();
		String pwdEncrypt = encoder.encode(password);
		user.setPassword(pwdEncrypt);
		user.setEnabled(1);
		
		serviceUser.save(user);
		
		Profile profile = new Profile();
		profile.setUsername(user.getUsername());
		profile.setProfileName(profileName);
		
		serviceProfile.save(profile);
		
		return "redirect:/users/index";
		
	}
	
	@GetMapping("/demo-bcrypt")
	public String demoBCrypt() {
		String password = "mari123";
		String encript = encoder.encode(password);
		System.out.println("Password encriptado: " + encript);
		return "users/demo";
	}
	
}
