package net.ronaldfdg.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.ronaldfdg.model.Banner;
import net.ronaldfdg.service.IBannerService;
import net.ronaldfdg.util.Utileria;

@Controller
@RequestMapping("/banners")
public class BannersController {

	@Autowired
	private IBannerService serviceBanner;

	@GetMapping("/index")
	public String index(@RequestParam("page") int numberPage, RedirectAttributes attribute, Model model) {
		
		if(numberPage < 0) {
			attribute.addFlashAttribute("messageError", "No puede buscar una p�gina menor a 0");
			return "redirect:/banners/index?page=0";
		}
		
		Pageable page = PageRequest.of(numberPage, 5, Sort.by("title"));
		Page<Banner> pageBanners = serviceBanner.getPagesBanner(page);
		
		if(numberPage >= pageBanners.getTotalPages()) {
			numberPage = pageBanners.getTotalPages()-1;
			attribute.addFlashAttribute("messageError", "No puede realizar una busqueda mayor a la p�gina "+numberPage);
			return "redirect:/banners/index?page="+String.valueOf(numberPage);
		}
		
		model.addAttribute("listBanners", pageBanners);
		return "banners/listBanners";
	}

	@GetMapping("/create")
	public String create(@ModelAttribute Banner banner) {
		return "banners/formBanner";
	}
	
	@GetMapping("/edit/{id}")
	public String edit(@PathVariable("id") int id, Model model, RedirectAttributes attribute) {
		
		if(!serviceBanner.existsById(id)) {
			attribute.addFlashAttribute("messageError", "No existe un banner con el id: "+id);
			return "redirect:/banners/index?page=0";
		}
		
		Banner banner = serviceBanner.findById(id);
		model.addAttribute("banner", banner);
		return "banners/formBanner";
	}
	
	@GetMapping("/delete/{id}")
	public String delete(@PathVariable("id") int id, RedirectAttributes attribute) {
		if(!serviceBanner.existsById(id)) {
			attribute.addFlashAttribute("messageError", "No existe un banner con el id: "+id);
			return "redirect:/banners/index?page=0";
		}
		
		serviceBanner.deleteById(id);
		attribute.addFlashAttribute("messageSuccess", "Se elimin� el banner correctamente");
		return "redirect:/banners/index?page=0";
	}

	@PostMapping("/save")
	public String save(@ModelAttribute Banner banner, Model model, BindingResult result ,RedirectAttributes attributes,
						@RequestParam("imageFile") MultipartFile multipart, HttpServletRequest request) {
		
		String messageSuccess = null;
		
		if(result.hasErrors()) {
			model.addAttribute("messageError", "Hubo alg�n problema al hacer el registro");
			return "banners/formBanner";
		}
		
		if (!multipart.isEmpty()) {
			String imageName = Utileria.saveImage(multipart, request);
			banner.setImage(imageName);
		}
		
		if(serviceBanner.existsById(banner.getId()))
			messageSuccess =  "Banner updated successfully";
		else
			messageSuccess = "Banner saved successfully";
		
		serviceBanner.save(banner);
		attributes.addFlashAttribute("messageSuccess", messageSuccess);
		return "redirect:/banners/index?page=0";
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

	public void validateMultipart(MultipartFile multipart, HttpServletRequest request, Banner banner) {
		String imageName = "";
		if (!multipart.isEmpty()) {
			imageName = Utileria.saveImage(multipart, request);
			banner.setImage(imageName);
		} else
			System.out.print("WARNING: Image file empty");
	}

}
