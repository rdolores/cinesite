package net.ronaldfdg.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.ronaldfdg.model.Movie;
import net.ronaldfdg.service.IDetailService;
import net.ronaldfdg.service.IMovieService;
import net.ronaldfdg.util.Utileria;

@Controller
@RequestMapping("/movies")
public class MoviesController {

	@Autowired
	private IDetailService serviceDetail;

	@Autowired
	private IMovieService serviceMovie;

	@GetMapping(value = "/create")
	public String create(@ModelAttribute Movie movie) {
		return "movies/formMovie";
	}

	@GetMapping("/index")
	public String indexMovie(@RequestParam("page") int numberPage, RedirectAttributes attribute, Model model) {
		
		if(numberPage < 0) {
			attribute.addFlashAttribute("mensajeError", "No puede realizar una busqueda menor a 0");
			return "redirect:/movies/index?page=0";
		}
		
		Pageable page = PageRequest.of(numberPage, 5, Sort.by("titulo").ascending());
		Page<Movie> listMovies = serviceMovie.getMoviesForPage(page);
		
		if(numberPage >= listMovies.getTotalPages()) {
			numberPage = listMovies.getTotalPages()-1;
			attribute.addFlashAttribute("mensajeError", "El total m�ximo de p�ginas que se pueden recorrer son: "+numberPage);
			return "redirect:/movies/index?page="+String.valueOf(numberPage);
		}
		
		model.addAttribute("listMovies", listMovies);
		return "movies/listMovies";
	}

	@PostMapping(value = "/save")
	public String save(@ModelAttribute Movie movie, BindingResult result, RedirectAttributes attributes,
			@RequestParam("archivoImagen") MultipartFile multipart, HttpServletRequest request) {

		String mensaje = null;

		if (result.hasErrors())
			return "movies/formMovie";

		if (!multipart.isEmpty()) {
			String imageFile = Utileria.saveImage(multipart, request);
			movie.setImagen(imageFile);
		}

		if (serviceMovie.existMovie(movie.getId()))
			mensaje = "Se actualiz� la pelicula correctamente";
		else
			mensaje = "Se realiz� el registro correctamente";

		serviceDetail.save(movie.getDetail());

		serviceMovie.save(movie);

		attributes.addFlashAttribute("mensaje", mensaje);

		return "redirect:/movies/index?page=0";
	}

	@GetMapping(value = "/delete/{idMovie}")
	public String delete(@PathVariable("idMovie") int idMovie, RedirectAttributes attributes) {
		
		if(serviceMovie.existMovie(idMovie)) {
			Movie movie = serviceMovie.getMovieById(idMovie);
			serviceMovie.deleteById(idMovie);
			serviceDetail.delete(movie.getDetail().getId());
			attributes.addFlashAttribute("mensaje", "Se elimin� el registro correctamente");
		} else 
			attributes.addFlashAttribute("mensajeError", "No se pudo eliminar la pelicula. No existe!");
		

		return "redirect:/movies/index?page=0";
	}

	@GetMapping(value = "/edit/{idMovie}")
	public String edit(@PathVariable("idMovie") int idMovie, Model model) {
		Movie movie = serviceMovie.getMovieById(idMovie);
		model.addAttribute("movie", movie);
		return "movies/formMovie";
	}

	@ModelAttribute("kindOfMovies")
	public List<String> getListKindOfMovies() {
		return serviceMovie.getKindOfMovies();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
	}

}
