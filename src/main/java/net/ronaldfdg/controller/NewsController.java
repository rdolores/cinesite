package net.ronaldfdg.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.ronaldfdg.model.News;
import net.ronaldfdg.service.INewsService;

@Controller
@RequestMapping("/news")
public class NewsController {

	@Autowired
	private INewsService serviceNews;

	@GetMapping(value = "/index")
	public String index(Model model) {
		List<News> listNews = serviceNews.getListNews();
		model.addAttribute("listNews", listNews);
		return "news/listNew";
	}

	@GetMapping(value = "/create")
	public String create(@ModelAttribute News news) {
		return "news/formNew";
	}

	@PostMapping(value = "/save")
	public String save(@ModelAttribute News news, RedirectAttributes attribute) {

		if (serviceNews.existNews(news.getId()))
			attribute.addFlashAttribute("mensaje", "Se actualiz� la noticia correctamente");
		else
			attribute.addFlashAttribute("mensaje", "Se registr� la noticia correctamente");

		serviceNews.save(news);

		return "redirect:/news/index";
	}

	@GetMapping(value = "/edit/{idNews}")
	public String edit(@PathVariable("idNews") int idNews, Model model) {
		News news = serviceNews.getNewsById(idNews);
		model.addAttribute("news", news);
		return "news/formNew";
	}

	@GetMapping(value = "/delete/{idNews}")
	public String delete(@PathVariable("idNews") int idNews, RedirectAttributes attribute) {

		String mensaje = null;
		String mensajeError = null;

		if (serviceNews.existNews(idNews)) {
			serviceNews.deleteById(idNews);
			mensaje = "Se elimin� la noticia";
			attribute.addFlashAttribute("mensaje", mensaje);
		}

		if (mensaje == null) {
			mensajeError = "No se pudo eliminar la noticia. No existe!";
			attribute.addFlashAttribute("mensajeError", mensajeError);
		}
		
		return "redirect:/news/index";
	}

}
