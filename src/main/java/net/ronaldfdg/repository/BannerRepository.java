package net.ronaldfdg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.ronaldfdg.model.Banner;

@Repository
public interface BannerRepository extends JpaRepository<Banner, Integer> {

}
