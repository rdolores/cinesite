package net.ronaldfdg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.ronaldfdg.model.Profile;

@Repository
public interface ProfileRepository extends JpaRepository<Profile, Integer> {

}
