package net.ronaldfdg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.ronaldfdg.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {

}
