package net.ronaldfdg.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.ronaldfdg.model.News;

@Repository
public interface NewsRepository extends JpaRepository<News, Integer> {

	List<News> findByEstatus(String estatus);
	List<News> findByDateOfNew(Date date);
	List<News> findByEstatusAndDateOfNew(String estatus, Date date, Sort sort);
	List<News> findByEstatusOrDateOfNew(String estatus, Date date);
	List<News> findByDateOfNewBetween(Date startDate, Date endDate);
	List<News> findTop3ByEstatusOrderByDateOfNewDesc(String estatus);
	
}
