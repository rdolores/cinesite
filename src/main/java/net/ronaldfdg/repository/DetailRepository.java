package net.ronaldfdg.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.ronaldfdg.model.Detail;

@Repository
public interface DetailRepository extends JpaRepository<Detail, Integer> {

}
